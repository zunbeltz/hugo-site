module gitlab.com/pages/hugo

go 1.20

require (
	github.com/halogenica/beautifulhugo v0.0.0-20240507175615-5bb2aa164640 // indirect
	github.com/theNewDynamic/gohugo-theme-ananke v0.0.0-20231122160523-91df000ca827 // indirect
)

// require  github.com/razonyang/hugo-theme-bootstrap v1.5.6 //indirect
// require github.com/halogenica/beautifulhugo v0.0.0 // indirect
