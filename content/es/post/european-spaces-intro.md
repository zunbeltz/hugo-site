---
Title: Explorando los espacios de datos europeos
Date: 2024-06-07T08:21:54+02:00
Series: ['dataspaces']
Tags: [Europa, Tecnología, Espacios de datos, dataspaces]
Draft: false
---

[MIK S.Coop.](https://mik.mondragon.edu/es/inicio) está desarrollando un proyecto con [Eustat](https://www.eustat.eus/indice.html), para poner, con mayor facilidad, 
su catalogo de datos al servicio de los investigadores. La tecnología que estamos utilizando para
compartir los datos es la misma que se está desarrollando para crear los [Espacios comunes europeos de datos]().

El proyecto ha sido reconocido por la asociación [BAIDATA](https://www.baidata.eu/), por su papel pionero en la adopción e implementación de tecnologías de espacios de datos.

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:share:7194369495375765504" height="200" width="704" frameborder="0" allowfullscreen="" title="Publicación integrada"></iframe>

Pero, empecemos por el principio.

# ¿Qué son los Espacios de Datos Europeos?

En la era digital, los datos son un recurso invaluable que impulsa la innovación y el progreso en todas las áreas de la sociedad. En Europa, la iniciativa de [Espacios de Datos Europeos](https://digital-strategy.ec.europa.eu/en/policies/strategy-data) está en el centro de este impulso hacia 

Los Espacios de Datos Europeos son una estrategia integral destinada a promover el acceso, intercambio, reutilización y mercado de datos en toda la Unión Europea. Este enfoque tiene como objetivo desbloquear el potencial de los datos para impulsar la innovación, el crecimiento económico y la creación de valor agregado para los ciudadanos europeos.

# Principios de desarrollo

## Marco Legal

La creación de un marco legal a través del [Data Act](https://digital-strategy.ec.europa.eu/en/factpages/data-act-explained) armonizado es fundamental para garantizar la protección de datos, la privacidad y la seguridad, al tiempo que se fomenta la interoperabilidad y la accesibilidad de los datos.

## Compartición de datos de manera confiable

La tecnología debe permitir la compartición de datos de manera que pueda haber confianza entre los agentes. El propietario de los datos los comparte de manera soberana. 

## Infraestructura Tecnológica

Se está desarrollando una infraestructura tecnológica robusta para facilitar el intercambio de datos de manera segura y eficiente. Esto incluye el desarrollo de [estándares comunes](https://dataroots.io/blog/data-space) y herramientas interoperables.

## Cultura de Datos Abiertos

Fomentar una cultura de [datos abiertos](https://data.europa.eu/en) es esencial para promover la transparencia, la participación ciudadana y la innovación. Esto implica la sensibilización, la capacitación y la promoción de prácticas de datos abiertos en todos los sectores.

## Espacios sectoriales

Varias iniciativa están desarrollando espacios de datos en diversos sectores, a través de acciones publico-privadas. Los espacios de datos permiten conectar agentes de la cadena de valor del sector. De esta manera, dichos agentes pueden intercambiar datos e incluso crear un *mercado de datos* que permita a las partes innovar. 

<table>
<tbody>
<tr>
<td><span lang="ES"><strong>Agricultura</strong></span></td>
<td>
<p><a href="https://agridataspace-csa.eu/"><span>AgriDataSpace</span></a><span>,&nbsp;</span><a href="https://divine-project.eu/"><span>Divine</span></a><span>,&nbsp;</span><a href="https://cracksense.eu/"><span>CrackSense</span></a><span>,&nbsp;</span><a href="https://scaleagdata.eu/en"><span>ScaleAgData</span></a><span>,&nbsp;</span><a href="https://agridatavalue.eu/"><span>AgDataValue</span></a><span>,</span></p>
<p><span lang="ES">4Growth,&nbsp;Dig4Live</span></p>
</td>
</tr>
<tr>
<td><span lang="ES"><strong>Patrimonio cultural </strong></span></td>
<td><a href="https://pro.europeana.eu/page/common-european-data-space-for-cultural-heritage"><span>Europeana pro</span></a><span>,&nbsp;</span><a href="https://eureka3d.eu/"><span lang="FR-BE">Eureka3D</span></a><span>,&nbsp;</span><a href="https://5dculture.eu/"><span lang="FR-BE">5Dculture</span></a><span>,&nbsp;</span><a href="https://pro.europeana.eu/project/de-bias"><span lang="FR-BE">DE-BIAS</span></a><span>,&nbsp;</span><a href="https://pro.europeana.eu/project/ai4europeana-an-ai-platform-for-the-cultural-heritage-data-space"><span lang="ES">AI4Europeana</span></a></td>
</tr>
<tr>
<td><span lang="ES"><strong>Energía</strong></span></td>
<td><a href="https://intnet.eu/"><span class="MsoHyperlink">IntNET</span></a><span class="MsoHyperlink">,&nbsp;</span><a href="https://omega-x.eu/"><span lang="FR-BE">OMEGA-X</span></a><span lang="FR-BE">,&nbsp;</span><a href="https://eddie.energy/"><span lang="FR-BE">EDDIE</span></a><span>,&nbsp;</span><a href="https://enershare.eu/"><span lang="FR-BE">Enershare</span></a><span>,&nbsp;</span><a href="https://energydataspaces.eu/"><span lang="FR-BE">Synergies</span></a><span lang="FR-BE">,&nbsp;</span><a href="https://datacellarproject.eu/"><span lang="ES">Data cellar</span></a></td>
</tr>
<tr>
<td><span lang="ES"><strong>Finanzas</strong></span></td>
<td><span>Compra pública bajo el programa Digital Europe&nbsp;(en&nbsp;desarrollo)&nbsp;</span></td>
</tr>
<tr>
<td><span lang="ES"><strong>Pacto Verde</strong></span></td>
<td>
<p><a href="https://www.greatproject.eu/"><span class="MsoHyperlink">GREAT</span></a><span class="MsoHyperlink">,&nbsp;</span><a href="https://ad4gd.eu/"><span>AD4GD</span></a><span lang="ES">,&nbsp;</span><a href="https://b-cubed.eu"><span>B-Cubed</span></a><span>,&nbsp;</span><a href="https://fairicube.nilu.no/"><span>FAIRiCUBE</span></a><span>,&nbsp;</span><a href="https://www.usage-project.eu/home"><span lang="ES">USAGE</span></a><span lang="ES">,&nbsp;</span></p>
<p><strong>Ciudades y comunidades inteligentes</strong></p>
<p><a href="https://www.ds4sscc.eu/"><span lang="ES">DS4SSCC</span></a></p>
<p><span lang="ES">DS4SSCC-DEP&nbsp;</span><span>(en&nbsp;desarrollo)&nbsp;</span></p>
</td>
</tr>
<tr>
<td><span><strong>Salud</strong></span></td>
<td>
<p><span><strong>European Health Data Space:</strong></span><br><a href="https://health.ec.europa.eu/ehealth-digital-health-and-care/electronic-cross-border-health-services_en"><span>MyHealth@EU</span></a></p>
<p><a href="https://ehds2pilot.eu/"><span>HealthData@EU Pilot</span></a></p>
<p><a href="https://tehdas.eu/"><span>Joint Action Towards the European Health Data Space – TEHDAS</span></a></p>
<p><span><strong>Cancer images:</strong></span></p>
<p><a href="https://cancerimage.eu/"><span>EUCAIM</span></a></p>
<p><span><strong>Genomics:</strong></span></p>
<p><a href="https://gdi.onemilliongenomes.eu/"><span>GDI</span></a></p>
</td>
</tr>
<tr>
<td><span lang="ES"><strong>Lenguaje</strong></span></td>
<td><a href="https://language-data-space.ec.europa.eu/index_en"><span>European language data space</span></a></td>
</tr>
<tr>
<td><span lang="ES"><strong>Manufactura</strong></span></td>
<td>
<p><a href="https://manufacturingdataspace-csa.eu/"><span>Data Space 4.0</span></a></p>
<p><a href="https://sm4rtenance.eu/"><span>SM4RTENANCE</span></a></p>
<p><a href="https://underpinproject.eu/"><span>UNDERPIN</span></a></p>
</td>
</tr>
<tr>
<td><span><strong>Media</strong></span></td>
<td><a href="https://tems-dataspace.eu/"><span>TEMS</span></a></td>
</tr>
<tr>
<td><span lang="ES"><strong>Movilidad</strong></span></td>
<td>
<p><a href="https://mobilitydataspace-csa.eu/"><span>PrepDSpace4Mobility</span></a></p>
<p><a href="https://deployemds.eu/"><span>deployEMDS</span></a></p>
</td>
</tr>
<tr>
<td><span lang="ES"><strong>Administración Pública</strong></span></td>
<td>
<p><span>Legal&nbsp;(en&nbsp;desarrollo)&nbsp;</span></p>
<p><span>OOTS - Once Only Technical System (en&nbsp;desarrollo)&nbsp;</span></p>
<p><span>Contratación pública: </span><a href="https://single-market-economy.ec.europa.eu/single-market/public-procurement/digital-procurement/public-procurement-data-space-ppds_en"><span>PPDS</span></a></p>
</td>
</tr>
<tr>
<td><span lang="ES"><strong>Investigación e Innovación</strong></span></td>
<td>
<p><a href="https://eosc-portal.eu/"><span>The European Open Science Cloud (EOSC)</span></a><span>,</span></p>
<p><a href="https://www.skills4eosc.eu/"><span>S</span>kills4EOSC</a>,&nbsp;<a href="https://eosc.eu/eosc-focus-project">EOSC Focus</a>, <a href="https://fair-impact.eu/">FAIR-IMPACT</a>,&nbsp;<a href="https://www.rd-alliance.org/get-involved/calling-rda-community/rda-tiger">RDA&nbsp;TIGER</a>,&nbsp;<a href="https://faircore4eosc.eu/">FAIRCORE4EOSC</a>,&nbsp;<a href="https://ai4eosc.eu/">AI4EOSC</a>,&nbsp;<a href="https://galaxyproject.org/projects/esg/">EuroScienceGateway</a>,&nbsp;</p>
<p><a href="https://www.fairease.eu/">FAIR-EASE</a>,&nbsp;<a href="https://raise-science.eu/">RAISE</a>,&nbsp;<a href="https://scilake.eu/">SciLake</a>,&nbsp;<a href="https://eosc4cancer.eu/">EOSC4Cancer</a>,&nbsp;<a href="https://graspos.eu/">GraspOS</a>,&nbsp;<a href="https://www.craft-oa.eu/">CRAFT-OA</a>,&nbsp;<a href="https://aquainfra.eu/">AquaINFRA</a>,&nbsp;<a href="https://blue-cloud.org/">Blue-Cloud 2026</a>,&nbsp;<a href="https://oscars-project.eu/">OSCARS</a>,&nbsp;<a href="https://everse.software/">EVERSE</a>,&nbsp;OSTrails,&nbsp;<a href="https://www.openaire.eu/eosc-beyond">EOSC Beyond</a>,&nbsp;<a href="http://eosc-entrust.eu/">EOSC-ENTRUST</a>,&nbsp;SIESTA,&nbsp;TITAN</p>
</td>
</tr>
<tr>
<td><span lang="ES"><strong>Skills</strong></span></td>
<td>
<p><a href="https://www.skillsdataspace.eu/"><span>DS4Skills</span></a></p>
<p><span>EDGE-Skills (en&nbsp;desarrollo)</span></p>
</td>
</tr>
<tr>
<td><span lang="ES"><strong>Turismo</strong></span></td>
<td>
<p><a href="https://tourism4-0.org/t4-0-projects/dates/"><span>DATES</span></a></p>
<p><a href="https://dsft.modul.ac.at/"><span>DFST</span></a></p>
</td>
</tr>
</tbody>
</table>

# Beneficios y Desafíos

Los Espacios de Datos Europeos ofrecen una serie de beneficios potenciales, como el impulso a la innovación, la mejora de los servicios públicos y la creación de nuevos modelos de negocio. 
Una prueba de la colaboración entre diferentes agentes, tanto públicos como privados, puede verse en el [Data Spaces Business Alliance Hubs](https://data-spaces-business-alliance.eu/).

Sin embargo, también plantean desafíos en diferentes dimensiones; como la gobernanza, la interoperabilidad y la protección de datos. El desarrollo, tanto de regulación como de tecnología, permitirá hacer frente a estos desafíos.
