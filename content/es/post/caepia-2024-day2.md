---
Title: CAEPIA2024 - Día 2
Date: 2024-06-20    
Lastmod: 2024-06-23T15:37:04+02:00
Series: ['CAEPIA2024']
Tags: [Conferencia, CAEPIA, IA]
Draft: false
---

![Vista de A Coruña](caepia2024-vistacoruña.jpg)


La jornada de hoy de la [Conferencia de la Asociación Española para la Inteligencia Artificial (CAEPIA)](https://caepia24.aepia.org/inicio.html) ha sido la principal en cuanto a charlas, con sesiones sobre «Visión Artificial Y Robótica», «Inteligencia Artificial Explicable y Responsable», «Procesamiento de Lenguaje Natural,» «Aprendizaje Automático,» «Búsqueda y Optimización» y «Fundamentos, modelos y aplicaciones de IA,» entre muchos otros.

A continuación voy a describir brevemente algunas de las presentaciones de las sesiones que he podido atender y que más interesantes me parecen:

# Inteligencia Artificial Explicable y Responsable

> Jesús María Pérez, Olatz Arbelaitz y Javier Muguerza. 
> *Driven PCTBagging: Seeking greater discriminating capacity for the same level of interoperability.*

Txus Pérez, del grupo [Aldapa](https://www.aldapa.eus/es/) de la UPV/EHU ha presentado un algoritmo de clasificación basado en árboles que auna la eficacia del método bagging con la explicabilidad de un único árbol. En lugar de utilizar el sistema de votación del bagging en la decisión final de clasificación; se utiliza en la selección de variables a la hora de construir los árboles. De esta manera, se crean una única estructura, fácil de explicar. Estos son los llamados *árboles consolidados.* El método PCTBagging solo consolida los árboles hasta cierta profundidad (parámetro controlable) y a partir de esa profundidad, los diferentes árboles crecen como en el método bagging. 

# Procesamiento de Lenguaje Natural

> [Irene Sanchez-Montejo](https://www.linkedin.com/in/irene-s%C3%A1nchez-montejo-b1b492217/?originalSubdomain=es), Carlos Telleria-Oriols y Raquel Trillo-Lado. 
> Evaluation of language models for the study of similarity between medical diagnoses in Spanish*

Irene Sánchez-Montejo, del Instituto Aragonés de Salud, ha presentado un interesante uso de LLMs en atención primaria. A menudo, la descripción que presenta el médico del diagnóstico realizado en una visita del paciente, no concuerda con el código de patología asignado en el manual que se utiliza en el Instituto Aragonés de Salud (esto puede deberse a cambios en el diagnóstico a medida que se desarrolla el mismo, no pudiéndose cambiar el código inicial asignado)

Esto no es un problema para los médicos, porque siempre se rigen por los descriptivos escritos. Sin embargo, si se quieren realizar estudios estadísticos, surgen problemas al utilizarse los códigos asignados.
El trabajo muestra el uso de LLMs preentrenados con corpus médicos, para derivar el código de patología correcto a partir del descriptivo.

# Aprendizaje Automático

> [Maitane Martinez-Eguiluz](https://www.linkedin.com/in/maitane-martinez-eguiluz-3515381b7/?original_referer=https%3A%2F%2Fwww%2Egoogle%2Ecom%2F&originalSubdomain=es), [Javier Muguerza](https://www.linkedin.com/in/javier-muguerza-rivero-29278b2b/?originalSubdomain=es), Olatz Arbelaitz, Ibai Gurrutxaga, Juan Carlos Gomez-Esteban, Ane Murueta-Goyena  Iñigo Gabilondo. 
> Predicting Parkinson’s Disease Progression: Analyzing Prodromal Stages through Machine Learning*

Javier Muguerza ha presentado, en sustitución de Maitane Martínez, un interesante trabajo de predecir el diagnostico de Parkinson a 7 años vista. Años antes de la aparición de los síntomas externos más conocidos del Parkinson, los pacientes presentan síntomas leve relacionados con el sistema olfativo y otros síntomas cognitivos leves. 

Utilizando métodos de Suport Vector Machine (SVM) han conseguido predecir con un 85% de *accuracy* el diagnóstico de Parkinson, con los datos de la primera visita, con siete años de antelación. Los datos utilizados para el estudio parten de la Fundación Michael J. Fox.

Con técnicas SHAP de explicabilidad, se ha encontrado que uno de los parámetros que mayor peso tiene en la predicción positiva es la dilatación del tercer ventrículo que presentan las resonancias. La importancia de este factor, por ser un *proxy* para tener en cuenta la perdida de tamaño de las zonas adyacentes; ha sido corroborado por los expertos.

> [Jacobo Ayensa-Jiménez,](https://i3a.unizar.es/es/investigadores/jacobo-ayensa-jimenez) Denis Navarro, Andrés Mena Tobar, Isabel Marquina, Sofía Hakim, Jorge Alfaro, Manuel Doblaré y Ángel Borque-Fernando. 
> Classification of Prostate Cancer Histopathological Structures by means of Deep Learning*

Jacobo Ayesa, de la Universidad de Zaragoza, ha mostrado un modelo para clasificar imágenes de pruebas de histología, mediante aprendizaje profundo. El proceso de etiquetado de las imágenes es un trabajo duro (que han realizado numerosos patólogos colaboradores) y no es sencillo, marcar la lesión perfectamente. Por este motivo, un proceso de segmentación para identificar zonas que puedan diagnosticar positivamente el cáncer de próstata no es viable. 

El método utilizado en este trabajo ha consistido en dividir la imagen en diferentes parches, e intentar clasificar cada parche a alguna de las 14 clases distintas de lesiones que puede mostrar el cáncer de prostata. 

> [Carlos Cernuda](https://www.linkedin.com/in/carlos-cernuda-1676412a/?original_referer=https%3A%2F%2Fwww%2Egoogle%2Ecom%2F&originalSubdomain=es) y Arkaitz Bidaurrazaga. 
> Non-linearly projected sample-similarity graph-based feature extractor for classification and regression problems.*

El compañero de MU—Goi Eskola Politeknikoa, Carlos Cernuda, ha presentado un método para extraer las características más relevantes de un problema, utilizando la similitud entre las filas de datos.

Para ello se crea un grafo, donde cada nodo es la fila de datos y las características del nodo, los datos en sí. Utilizando una red neuronal de grafos (GNN), se encuentran similitudes entre nodos a partir de las similitudes entre los *embeddings* (creados por la GNN) de cada nodo.

# Fundamentos, modelos y aplicaciones de IA

> [Amaia Abanda,](https://www.linkedin.com/in/amaiaabanda/?original_referer=https%3A%2F%2Fwww%2Egoogle%2Ecom%2F&originalSubdomain=es) Ainhoa Pujana y Javier del Ser. 
> Reconstruction-based Anomaly Detection
in Wind Turbine Operation Time Series using Generative Models

Amaia Abanda, de Tecnalia, ha mostrado el trabajo realizado para la detección de anomalías en el funcionamiento de los sistemas SCADA de aerogeneradores. En este trabajo se ha entrenado redes neuronales con una *sliding windows* de 4 años de datos para reconstruir la series temporales. Estas redes *aprenden* la normalidad del sistema y son capaces de detectar la situación anómala.

**Nota:** Más información sobre algunos de los trabajos presentados puede encontrarse en las [actas del Congreso.](https://caepia24.aepia.org/inicio_files/XX%20Caepia_2024_Actas_General.pdf)

# Charla plenaria

Después de las sesiones se ha presentado un vídeo conmemorativo de los 40 años de AEPIA. Miembros de la Asociación han recordado la historia de la misma, desde su fundación. Ha servido como emotivo recuerdo a los miembros que ya no están.

Al finalizar, la profesora [Amparo Alonso](https://www.linkedin.com/in/amparo-alonso-80516211/?originalSubdomain=es) ha impartido una charla plenaria donde ha incidido en la necesidad de tener en cuenta la sostenibilidad en la construcción y uso de modelos de IA. Se estima que para 2030 la IA consumirá el 30% de la energía eléctrica. 

La profesora ha mostrado diferentes trabajos que se han realizado en su grupo. Se está trabajando, entre otros, en 3 dimensiones:
* Mejorar la calidad de datos para poder reducir los modelos y así su consumo. 
* Reducir la dimensionalidad con nuevas técnicas
* Modelos más reducidos utilizando datos de menor precisión (32, 16 u 8 bits) sin perder poder predictivo.

# Cena de gala

El día ha terminado con la cena de gala. Esta ha tenido lugar en el hotel NH Finisterre, con vistas al puerto y no muy lejos de la Torre de Hércules.

![Salón cena](caepia2024-cena.jpg)

