---
Title: CAEPIA2024 - Día 3
Date: 2024-06-21    
Lastmod: 2024-07-02T20:00:04+02:00
Series: ['CAEPIA2024']
Tags: [Conferencia, CAEPIA, IA]
Draft: false
---

La jornada de hoy de la [Conferencia de la Asociación Española para la Inteligencia Artificial (CAEPIA)](https://caepia24.aepia.org/inicio.html) ha sido la última. 

Después de 2 sesiones de charlas se ha celebrado la asamblea de AEPIA, y se han entregado los premios en la ceremonia de clausura. Por horario de transporte, solo he podido asistir a las presentaciones. Aquí van alguna de las más interesantes:

![Panorámica Coruña](caepia2024-panoramica.jpg)
(Fuente: https://www.attica21hotels.com/que-ver-en-la-coruna-en-un-dia/)

# Inteligencia Artificial Explicable y Responsable

> [Aránzazu Jurío](https://www.unavarra.es/pdi?uid=810397), Rubén Pascual, Iris Dominguez-Catena, Daniel Paternain y Mikel Galar. *Diseño y Captura de una Base de Datos para el Reconocimiento de Emociones Minimizando Sesgos*

Aránzazu Jurío, de la UPNA, ha presentado el trabajo realizado para crear un base de datos de expresiones faciales de diferentes sentimientos, representando proporcionalmente a los diferentes sexos y razas para evitar el sesgo representativo; y además, todas las personas han emulado *todos* los sentimientos para evitar el sesgo de estereotipo.  

Han recogido imágenes en presencial y en remoto a través de una app. Los resultado del análisis muestran métricas ENS, SEI y V de Cramer (https://github.com/irisdominguez/dataset_bias_metrics) acorde a la eliminación del sesgo en la base de datos creada.

# Inteligencia ambiental y entornos inteligentes

> Matheus Puime Pedra, Josune Hernantes, Leire Casals Baena y [Leire Labaka.](https://www.linkedin.com/in/leire-labaka-zubieta-4ba3511a/?originalSubdomain=es) *Windstorm Economic Impacts on the Spanish Resilience: A Machine Learning Real-Data Approach*

Leire Labaka, de Tecnum, ha presentado trabajo realizado por Matheus Puime, sobre la clasificación de diferentes daños causados por borrascas profundas (*windstrom*) utilizando modelos de *Random forests.*

La motivación principal del trabajo es poder medir la resiliencia, en sus distintas dimensiones, de ciudades. La Dr. Labaka ha remarcado la dificultad de obtener datos fiables y completos en desastres naturales; especial cuantificación completa de daños económicas sufridos y medios materiales y humanos utilizados en el momento de actuación.

**Nota:** Más información sobre algunos de los trabajos presentados puede encontrarse en las [actas del Congreso.](https://caepia24.aepia.org/inicio_files/XX%20Caepia_2024_Actas_General.pdf)


# Proxima CAEPIA 2026

En la asamblea se ha seleccionado la sede de la proxima conferencia: Jaén [(Acta de la asamblea)](https://www.aepia.org/wp-content/uploads/2024/07/acta_asamblea_2024.pdf)



