---
Title: CAEPIA2024 - Día 1
Date: 2024-06-19
Series: ['CAEPIA2024']
Tags: [Conferencia, CAEPIA, IA]
Draft: false
---

La [Asociación Española para la Inteligencia Artificial (CAEPIA)](https://caepia24.aepia.org/inicio.html) celebra su 40 aniversario con la edición XX de la conferencia bianual CAEPIA. Este año se celebra en La Coruña; ciudad que es sede de La Agencia Española de Supervisión de la Inteligencia Artificial.

![Vista de A Coruña](caepia2024-acoruña.jpg)

## Día 1

La jornada de hoy, 19 de junio, ha sido la inaugural. La ceremonia de apertura ha sido seguida por dos charlas plenarias. La primera, impartida por el profesor [Ramón López de Mántaras,](https://www.linkedin.com/in/ramonlopezdemantaras/) ha tratado  sobre algunas reflexiones en torno a la inteligencia de la Inteligencia Artificial y su comparación con la inteligencia humana.
La segunda, realizada por la profesora [Carme Torras](https://www.linkedin.com/in/carme-torras-274602232/), ha versado sobre Las múltiples facetas y desafíos de la robótica asistencial. No he podido llegar a estas dos primeras charlas por motivos de transporte, pero puedo imaginar que han sido de gran interes. 

Después de la comida, [José María Lasalle](https://www.linkedin.com/in/jos%C3%A9-mar%C3%ADa-lassalle-30ab601b3/?originalSubdomain=es) ha impartido una conferencia titulada *IA y autenticidad humana.* Ha recalcado que, probablemente, la IA vaya a ser la herramienta con el mayor impacto socio-cultural de la historia. Así como, la máquina de vapor permitido el paso del antiguo regimen al capitalismo industrial y al estado de derecho y bienestar de hoy en día; la IA nos está acercando a un capitalismo cognitivo, en la que el trabajo intelectual está siendo, paulatinamente, transferido de las personas a las máquina.

Este nuevo capitalismo está empobreciendo la clase media, y en consecuencia, es responsable del auge de la extrema derecha. La IA, como maximizador de la productividad, está aumentado la desigualdad entre los dueños y dirigentes de la empresas-plataformas y los trabajadores del conocimiento que desarrollan y hacen posible está misma IA. 

El profesor Lasalle propone incentivar el debate social sobre estos efectos que está generando el uso de la IA. Frente a los tecnólogos libertarios que abogan por un desarrollo y uso sin restricciones; más allá de las que pueda generar el mercado, se debe regular, desde una mirada de la ética de la responsabilidad y la ética de la autenticidad, para evitar abusos (como el control social o el uso de armas letales autónomas) y asegurar una redistribución de la riqueza generada.

A continuación se ha celebrado una mesa redonda denominada *Retos empresariales de la IA.* Ponentes de diferentes empresas; DataSpartan, Abanca, Esposible, Inditex, Minsait y NTTData, han hablado de sus últimos proyectos interesantes, la necesidad de llevar modelos de calidad a producción, la relación cliente-proveedor con las grandes empresas proveedoras de infraestructura, la falta de calidad de los datos en las empresas y el reto de adquirir talento, especialmente para las empresas de menor tamaño.

Por la noche se ha celebrado una romería con espectáculo de música y baile gallego y cena de empanada, pulpo y tarta de Santiago.

![Vista de la romería](caepia2024-romeria.jpg)
