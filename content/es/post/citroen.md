---

Title: ¡Odio los Citroën!

Date: 2024-08-21    

Lastmod: 2024-06-08T20:00:04+02:00

Series: ['Data science']

Tags: [analítica, data, datos]

Dfalse: false

---

Mi hijo de 8 años tiene un odio a los Citroën, que realmente no sé de donde viene. Dice que siempre ve muchos Citroëns por todos lados y que no le gustan 🤷‍♂️.

Un día de vacaciones, ¡se enfado porque el Uber que nos recogió era Citroën! Entonces le pregunte a ver cuantos Citroën creía que había. Respondió que de cada 100 coches habría uno 3 Citroën. 

Como científico de datos, no me quedo otra que buscar información 🔍.


En unos minutos descubrí que la DGT tiene una página de [*open data*](https://www.dgt.es/menusecundario/dgt-en-cifras/) donde se pueden descargar datos estadísticos. Entre otros, encontré un fichero de información sobre el parque automovilístico de España. Este fichero contiene, en principio, una lista de todos los coches dados de alta en España con fecha de 2023.

¡Es un fichero de texto (conn separador |) de 7.5 GB! Así que necesitaremos cierto cuidado para leer los datos. 
La cabecera del fichero es la siguiente:

`PROVINCIA|MUNICIPIO|FABRICANTE|MARCA|MODELO|TIPO|VARIANTE|VERSION|PROVINCIA_MATR|
FECHA_MATR|FECHA_PRIM_MATR|CLASE_MATR|PROCEDENCIA|NUEVO_USADO|TIPO_TITULAR|NUM_TITULARES|
SUBTIPO_DGT|TIPO_DGT|CAT_EURO|CLAS_CONSTRUCCION|CLAS_UTILIZACION|SERVICIO|RENTING|TARA
|PESO_MAX|MOM|MMTA|CILINDRADA|POTENCIA|KW|PROPULSION|CATELECT|CONSUMO|AUTONOMIA|ALIMENTACION|
TIPO_DISTINTIVO|EMISIONES_EURO|EMISIONES_CO2|CARROCERIA|DISTANCIA_EJES|EJE_ANTERIOR|
EJE_POSTERIOR|PLAZAS|PLAZAS_MAX|PLAZAS_PIE`

## ¿Cuantos Citroen hay realmente?

Nos va a interesar leer las variables `FABRICANTE`, `MARCA` y `TIPO_DGT` para contar los turismos de distintos fabricantes. Así mismo, leeremos las columnas `PROVINCIA` y `MUNICIPIO` para analizar si hay diferencias significativas entre distintas partes del estado. 


```python
import pandas as pd
```


```python
fields = ['FABRICANTE', 'MARCA', 'TIPO_DGT', 'PROVINCIA', 'MUNICIPIO']
data = pd.read_csv(PATH+'parque_consolidado_2023.txt', sep='|', usecols=fields)
```


```python
data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PROVINCIA</th>
      <th>MUNICIPIO</th>
      <th>FABRICANTE</th>
      <th>MARCA</th>
      <th>TIPO_DGT</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>14</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>HONDA</td>
      <td>MOTOCICLETAS</td>
    </tr>
    <tr>
      <th>1</th>
      <td>14</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>RENAULT</td>
      <td>TRACTORES INDUSTRIALES</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>4013.0</td>
      <td>JIANGSU DAFIER MOTORCYCLE CO., LTD</td>
      <td>HANWAY</td>
      <td>CICLOMOTORES</td>
    </tr>
    <tr>
      <th>3</th>
      <td>20</td>
      <td>20040.0</td>
      <td>NaN</td>
      <td>TOYOTA</td>
      <td>FURGONETAS</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>8019.0</td>
      <td>PORSCHE AG</td>
      <td>PORSCHE</td>
      <td>TURISMOS</td>
    </tr>
  </tbody>
</table>
</div>



Veamos la marcas más numerosas y las menos representadas.


```python
data['MARCA'].value_counts()
```




    MARCA
    RENAULT       2972577
    SEAT          2645079
    CITROEN       2574202
    PEUGEOT       2566820
    VOLKSWAGEN    2375255
                   ...   
    BODEN               6
    NICOLAS             6
    RAMPINI             6
    CAMPIÑA             6
    AM GENERAL          6
    Name: count, Length: 1888, dtype: int64



Como vemos nos interesa la `MARCA` CITROEN. Primero empezaremos por seleccionar los turismos.


```python
turismos = data[data['TIPO_DGT'] == 'TURISMOS']
```


```python
turismos.info(verbose=True)
```

    <class 'pandas.core.frame.DataFrame'>
    Index: 25356594 entries, 4 to 37890860
    Data columns (total 5 columns):
     #   Column      Dtype  
    ---  ------      -----  
     0   PROVINCIA   int64  
     1   MUNICIPIO   float64
     2   FABRICANTE  object 
     3   MARCA       object 
     4   TIPO_DGT    object 
    dtypes: float64(1), int64(1), object(3)
    memory usage: 1.1+ GB
    


```python
turismos['MARCA'].isnull().sum()
```




    0



Todos los turismos tienen una `MARCA` asignada.


```python
turismos_total = len(turismos)
turismos_total
```




    25356594



En España tenemos unos 25.3 millones de turismos. ¿Cuantos hay de cada marca?


```python
marca_count = turismos['MARCA'].value_counts()
marca_count
```




    MARCA
    SEAT              2591245
    RENAULT           2314288
    VOLKSWAGEN        2098815
    PEUGEOT           2023176
    CITROEN           1977853
                       ...   
    IGLUVAN                 1
    REWACO                  1
    MOTOR HISPANIA          1
    BERLIET                 1
    MORINI                  1
    Name: count, Length: 271, dtype: int64



Vemos que hay 271 marcas diferentes de turismos. Nos quedamos aquellas que tienen más de quinientos mil coches matriculados.


```python
marcas_famosas = marca_count[marca_count >= 5*10**5]
marcas_famosas
```




    MARCA
    SEAT             2591245
    RENAULT          2314288
    VOLKSWAGEN       2098815
    PEUGEOT          2023176
    CITROEN          1977853
    FORD             1695525
    OPEL             1548340
    MERCEDES-BENZ    1221725
    TOYOTA           1166514
    AUDI             1113963
    BMW               987462
    HYUNDAI           830400
    NISSAN            794748
    KIA               732419
    DACIA             565635
    FIAT              511034
    Name: count, dtype: int64




```python
((marcas_famosas.sum()/turismos_total) *100).round(1)
```




    87.4



Estas 16 marcas de turismos, representan el 87.4 % del total de parque de turismos.

De esta manera podemos calcular el porcentaje de cada marca:


```python
turismos_total = len(turismos)
((marcas_famosas/turismos_total) * 100).round(1)
```




    MARCA
    SEAT             10.2
    RENAULT           9.1
    VOLKSWAGEN        8.3
    PEUGEOT           8.0
    CITROEN           7.8
    FORD              6.7
    OPEL              6.1
    MERCEDES-BENZ     4.8
    TOYOTA            4.6
    AUDI              4.4
    BMW               3.9
    HYUNDAI           3.3
    NISSAN            3.1
    KIA               2.9
    DACIA             2.2
    FIAT              2.0
    Name: count, dtype: float64



Vemos que realmente hay un 7.8 % de Citroën, frente a un 10.2 % de SEAT y un 4.4 % de AUDI.

## ¿Hay diferencias entre provincias?

Primero, vamos a seleccionar solo los turismos de las marcas con mayor presencia en el parque y luego agruparemos por provincias.


```python
lista_marcas = marcas_famosas.index.to_list()
lista_marcas
```




    ['SEAT',
     'RENAULT',
     'VOLKSWAGEN',
     'PEUGEOT',
     'CITROEN',
     'FORD',
     'OPEL',
     'MERCEDES-BENZ',
     'TOYOTA',
     'AUDI',
     'BMW',
     'HYUNDAI',
     'NISSAN',
     'KIA',
     'DACIA',
     'FIAT']




```python
turismos_marcas_selecionadas = turismos[turismos['MARCA'].isin(lista_marcas)] 
```


```python
turismos_marcas_selecionadas['MARCA'].unique()
```




    array(['CITROEN', 'FORD', 'TOYOTA', 'MERCEDES-BENZ', 'SEAT', 'RENAULT',
           'VOLKSWAGEN', 'HYUNDAI', 'NISSAN', 'OPEL', 'KIA', 'BMW', 'PEUGEOT',
           'FIAT', 'AUDI', 'DACIA'], dtype=object)




```python
turismos_provincia_marca = turismos_marcas_selecionadas.groupby(['PROVINCIA', 'MARCA']).agg({'MARCA': 'count'})
turismos_provincia_marca.rename(columns={'MARCA': 'counts'}, inplace=True)
```


```python
turismos_provincia_marca.query('PROVINCIA == 48')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>counts</th>
    </tr>
    <tr>
      <th>PROVINCIA</th>
      <th>MARCA</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="16" valign="top">48</th>
      <th>AUDI</th>
      <td>26837</td>
    </tr>
    <tr>
      <th>BMW</th>
      <td>20647</td>
    </tr>
    <tr>
      <th>CITROEN</th>
      <td>42612</td>
    </tr>
    <tr>
      <th>DACIA</th>
      <td>11034</td>
    </tr>
    <tr>
      <th>FIAT</th>
      <td>6705</td>
    </tr>
    <tr>
      <th>FORD</th>
      <td>38598</td>
    </tr>
    <tr>
      <th>HYUNDAI</th>
      <td>14235</td>
    </tr>
    <tr>
      <th>KIA</th>
      <td>13457</td>
    </tr>
    <tr>
      <th>MERCEDES-BENZ</th>
      <td>21160</td>
    </tr>
    <tr>
      <th>NISSAN</th>
      <td>14385</td>
    </tr>
    <tr>
      <th>OPEL</th>
      <td>40175</td>
    </tr>
    <tr>
      <th>PEUGEOT</th>
      <td>39431</td>
    </tr>
    <tr>
      <th>RENAULT</th>
      <td>57728</td>
    </tr>
    <tr>
      <th>SEAT</th>
      <td>47472</td>
    </tr>
    <tr>
      <th>TOYOTA</th>
      <td>19883</td>
    </tr>
    <tr>
      <th>VOLKSWAGEN</th>
      <td>46804</td>
    </tr>
  </tbody>
</table>
</div>




```python
turismos_provincia_porcentaje = (turismos_provincia_marca['counts'] /turismos_provincia_marca.groupby('PROVINCIA')['counts'].transform('sum') * 100).round(1).to_frame()
turismos_provincia_porcentaje
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>counts</th>
    </tr>
    <tr>
      <th>PROVINCIA</th>
      <th>MARCA</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5" valign="top">1</th>
      <th>AUDI</th>
      <td>5.5</td>
    </tr>
    <tr>
      <th>BMW</th>
      <td>4.6</td>
    </tr>
    <tr>
      <th>CITROEN</th>
      <td>10.0</td>
    </tr>
    <tr>
      <th>DACIA</th>
      <td>1.9</td>
    </tr>
    <tr>
      <th>FIAT</th>
      <td>1.8</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="5" valign="top">52</th>
      <th>PEUGEOT</th>
      <td>7.0</td>
    </tr>
    <tr>
      <th>RENAULT</th>
      <td>7.2</td>
    </tr>
    <tr>
      <th>SEAT</th>
      <td>6.6</td>
    </tr>
    <tr>
      <th>TOYOTA</th>
      <td>4.3</td>
    </tr>
    <tr>
      <th>VOLKSWAGEN</th>
      <td>10.9</td>
    </tr>
  </tbody>
</table>
<p>832 rows × 1 columns</p>
</div>




```python
turismos_provincia_porcentaje.query("MARCA == 'CITROEN'")
```

Creamos una gráfica para comparar todos los valores.


```python
df = turismos_provincia_porcentaje.query("MARCA == 'CITROEN'").droplevel(1)
ax = df.plot.bar(y='counts', rot=0)
ax.get_legend().remove()
ax.set_ylabel('%', rotation=0)
ax.set_title('Porcentaje de Citroën por provincia')
for i, t in enumerate(ax.get_xticklabels()):
    if (i % 5) != 0:
        t.set_visible(False)
```


    
![png](./citroen_36_0.png)
    


## ¿Qué porcentaje hay en los distintos municipios de mi provincia?

Vamos a calcular la proporción en cada municipio de mi provincia y luego mostraremos un mapa en escala de color. Para dibujar el mapa he encontrado un paquete basado en `geopandas` para dibujar comunidades, provincias y municipios del estado: [geospain](https://github.com/MrCabss69/GeoSpain/)

Sin embargo, esta librería solo contiene los nombres de los municipios; y los datos de la DGT identifican los municipios por código postal. He encontrado la definición geoespacial de los municipios con los códigos postales en el repositorio [ds-codigos postales](https://github.com/inigoflores/ds-codigos-postales/tree/master/data) 
He descargado un fichero `geojson` con los datos.


```python
import geopandas as gp
municipios_geo = gp.read_file('VIZCAYA.geojson')
municipios_geo.info()
```

    <class 'geopandas.geodataframe.GeoDataFrame'>
    RangeIndex: 184 entries, 0 to 183
    Data columns (total 5 columns):
     #   Column      Non-Null Count  Dtype         
    ---  ------      --------------  -----         
     0   ID_CP       184 non-null    float64       
     1   COD_POSTAL  184 non-null    object        
     2   ALTA_DB     184 non-null    datetime64[ms]
     3   CODIGO_INE  184 non-null    int32         
     4   geometry    184 non-null    geometry      
    dtypes: datetime64[ms](1), float64(1), geometry(1), int32(1), object(1)
    memory usage: 6.6+ KB
    


```python
PROVINCIA = 48
turismos_48 = turismos.query('PROVINCIA == 48')
turismos_48.MUNICIPIO.unique()
```




    array([48029., 48020., 48027., 48044., 48085.,    nan, 48082., 48015.,
           48013., 48034., 48011., 48036., 48078., 48902., 48069., 48080.,
           48054., 48046., 48084., 48017., 48003.])



Como se puede ver, no hay datos en todos los códigos postales de la provincia de Bizkaia.


```python
print(turismos_48['MUNICIPIO'].isnull().sum())
print(len(turismos_48['MUNICIPIO']))
```

    119772
    526370
    

Una quinta parte de los turismos de Bizkaia no tienen asignado el código postal. Primero debemos eliminar estos registros.


```python
turismos_48 = turismos_48[turismos_48['MUNICIPIO'].notna()]
```


```python
print(turismos_48['MUNICIPIO'].isnull().sum())
print(len(turismos_48['MUNICIPIO']))
```

    0
    406598
    


```python
turismos_48 = turismos_48.astype({'MUNICIPIO': int})
```

Convertiremos los códigos postales a enteros en los datos geoespaciales para poder unir con la tabla que creemos de porcentaje de Citroën por municipio.


```python
municipios_geo = municipios_geo.astype({'COD_POSTAL': int})
```


```python
turismos_48_marca = turismos_48.groupby(['MUNICIPIO', 'MARCA']).agg({'MARCA': 'count'})
turismos_48_marca.rename(columns={'MARCA': 'counts'}, inplace=True)
turismos_48_porcentaje = (turismos_48_marca['counts'] /turismos_48_marca.groupby('MUNICIPIO')['counts'].transform('sum') * 100).round(1).to_frame()
turismos_48_porcentaje
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>counts</th>
    </tr>
    <tr>
      <th>MUNICIPIO</th>
      <th>MARCA</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="5" valign="top">48003</th>
      <th>ALFA ROMEO</th>
      <td>0.2</td>
    </tr>
    <tr>
      <th>AUDI</th>
      <td>5.0</td>
    </tr>
    <tr>
      <th>BMW</th>
      <td>4.6</td>
    </tr>
    <tr>
      <th>CHEVROLET</th>
      <td>0.3</td>
    </tr>
    <tr>
      <th>CHRYSLER</th>
      <td>0.1</td>
    </tr>
    <tr>
      <th>...</th>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th rowspan="5" valign="top">48902</th>
      <th>TESLA</th>
      <td>0.1</td>
    </tr>
    <tr>
      <th>TOYOTA</th>
      <td>3.4</td>
    </tr>
    <tr>
      <th>VOLKSWAGEN</th>
      <td>7.5</td>
    </tr>
    <tr>
      <th>VOLVO</th>
      <td>1.8</td>
    </tr>
    <tr>
      <th>¡</th>
      <td>2.2</td>
    </tr>
  </tbody>
</table>
<p>944 rows × 1 columns</p>
</div>




```python
turismos_48_citroen = turismos_48_porcentaje.query("MARCA == 'CITROEN'")
turismos_48_citroen = turismos_48_citroen.droplevel(1)
turismos_48_citroen
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>counts</th>
    </tr>
    <tr>
      <th>MUNICIPIO</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>48003</th>
      <td>9.0</td>
    </tr>
    <tr>
      <th>48011</th>
      <td>8.7</td>
    </tr>
    <tr>
      <th>48013</th>
      <td>9.6</td>
    </tr>
    <tr>
      <th>48015</th>
      <td>9.5</td>
    </tr>
    <tr>
      <th>48017</th>
      <td>7.2</td>
    </tr>
    <tr>
      <th>48020</th>
      <td>7.3</td>
    </tr>
    <tr>
      <th>48027</th>
      <td>7.5</td>
    </tr>
    <tr>
      <th>48029</th>
      <td>8.7</td>
    </tr>
    <tr>
      <th>48034</th>
      <td>11.7</td>
    </tr>
    <tr>
      <th>48036</th>
      <td>8.0</td>
    </tr>
    <tr>
      <th>48044</th>
      <td>6.1</td>
    </tr>
    <tr>
      <th>48046</th>
      <td>8.2</td>
    </tr>
    <tr>
      <th>48054</th>
      <td>7.6</td>
    </tr>
    <tr>
      <th>48069</th>
      <td>6.1</td>
    </tr>
    <tr>
      <th>48078</th>
      <td>9.3</td>
    </tr>
    <tr>
      <th>48080</th>
      <td>8.6</td>
    </tr>
    <tr>
      <th>48082</th>
      <td>10.9</td>
    </tr>
    <tr>
      <th>48084</th>
      <td>9.1</td>
    </tr>
    <tr>
      <th>48085</th>
      <td>7.9</td>
    </tr>
    <tr>
      <th>48902</th>
      <td>8.9</td>
    </tr>
  </tbody>
</table>
</div>



No tenemos datos para muchos de los municipios, incluido el mío 😔.


```python
merged_data = municipios_geo.merge(turismos_48_citroen, left_on = 'CODIGO_INE', right_on='MUNICIPIO', how='outer')
```


```python
ax = merged_data.plot('counts', legend=True, missing_kwds={
        "color": "lightgrey",
        "edgecolor": "red",
        "hatch": "///",
        "label": "Missing values",
    })
fig = ax.get_figure()
ax.set_axis_off()
fig.suptitle('Porcentaje de Citroën por código postal')
ax.set_title('(sobreados sin datos)');
```


    
![png](./citroen_53_0.png)
    

