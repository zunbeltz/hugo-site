---
title: "Sobre mí"
description: ""
disable_feed: true
---

{{< columns >}} <!-- begin columns block -->

<img src="headshot.png" alt="Myself!" width="35%" height="35%">

<---> <!-- magic separator, between columns -->

Hola :wave:: 

Mí nombre es Zunbeltz Izaola y soy un *data scientist* interesado
en todo tipo de aspectos de los proyectos de datos; especialmente la gobernanza y compartición de datos.



{{< /columns >}}

