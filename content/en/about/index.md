---
title: "About me"
description: ""
disable_feed: true
---
{{< columns >}}

<img src="headshot.png" alt="Myself!" width="35%" height="35%">

<---> <!-- magic separator, between columns -->


Hi :wave:! 

<---> <!-- magic separator, between columns -->

My name is Zunbeltz Izaola and I am an *data scientist* interested
in all kinds of aspects of data projects; especially data governance and sharing.

{{< /columns >}}